const sequelize = require('../db');
const {DataTypes} = require('sequelize');

const User = sequelize.define('user', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  email: {type: DataTypes.STRING, unique: true},
  password: {type: DataTypes.STRING},
  role: {type: DataTypes.STRING, defaultValue: 'USER'},
});

const Product = sequelize.define('product', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  name: {type: DataTypes.STRING, unique: true},
  img: {type: DataTypes.STRING},
  capacity: {type: DataTypes.INTEGER},
  capacityTypeId: {type: DataTypes.INTEGER},
  manufacturer: {type: DataTypes.STRING},
  expirationId: {type: DataTypes.INTEGER},
  manufacturingDate: {type: DataTypes.DATE},
  barcode: {type: DataTypes.INTEGER, unique: true},
  aisleId: {type: DataTypes.INTEGER},
  typeId: {type: DataTypes.INTEGER},
});

const Aisle = sequelize.define('aisle', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  aisleName: {type: DataTypes.STRING, unique: true},
});

const Type = sequelize.define('type', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  productType: {type: DataTypes.STRING, unique: true},
});

const AisleType = sequelize.define('aisletype', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
});

const Expiration = sequelize.define('expiration', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  expirationType: {type: DataTypes.STRING, unique: true},
});

const CapacityTypes = sequelize.define('volumeTypes', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  capacityType: {type: DataTypes.STRING, unique: true},
});

Aisle.hasMany(Product);
Product.belongsTo(Aisle);

Type.hasMany(Product);
Product.belongsTo(Type);

Aisle.belongsToMany(Type, {through: AisleType});
Type.belongsToMany(Aisle, {through: AisleType});

Expiration.hasMany(Product);
Product.belongsTo(Expiration);

CapacityTypes.hasMany(Product);
Product.belongsTo(CapacityTypes);

module.exports = {
  User,
  Product,
  Aisle,
  Type,
  AisleType,
  Expiration,
  CapacityTypes,
};