const {Type} = require('../models/models');
const ApiError = require('../error/ApiError');

class TypeController {
  async create(req, res) {
    const {productType} = req.body;
    const type = await Type.create({productType});
    res.json(type);
  }

  async getAll(req, res) {
    const types = await Type.findAll();
    return res.json(types);
  }

  async delete(req, res) {
    
  }
};

module.exports = new TypeController;