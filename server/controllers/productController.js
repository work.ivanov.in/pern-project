const { Product } = require("../models/models");
const ApiError = require("../error/ApiError");
const uuid = require("uuid");
const path = require("path");

class ProductController {
  async create(req, res, next) {
    try {
      const {
        name,
        capacity,
        capacityTypeId,
        manufacturer,
        expirationId,
        manufacturingDate,
        barcode,
        aisleId,
        typeId,
      } = req.body;
      const img = req.files;
      let fileName = uuid.v4() + ".jpg";
      img.mv(path.resolve(__dirname, "..", "static", fileName));

      const product = await Product.create({
        name,
        img: fileName,
        capacity,
        capacityTypeId,
        manufacturer,
        expirationId,
        manufacturingDate,
        barcode,
        aisleId,
        typeId,
      });

      return res.json(product);
    } catch (error) {
      next(ApiError.badRequest(error.message));
    }
  }

  async getAll(req, res) {
    const products = await Product.findAll();
    return res.json(products);
  }

  async getOne(req, res) {
    const {id} = req.params;
    const product = await Product.findOne({where: id});
    return res.json(product);
  }

  async delete(req, res) {}
}

module.exports = new ProductController();
