const Router = require('express');
const router = new Router();
const aisleRouter = require('./aisleRouter');
const capacityRouter = require('./capacityRouter');
const expirationRouter = require('./expirationRouter');
const productRouter = require('./productRouter');
const typeRouter = require('./typeRouter');
const userRouter = require('./userRouter');


router.use('/user', userRouter);
router.use('/aisle', aisleRouter);
router.use('/capacity', capacityRouter);
router.use('/expiration', expirationRouter);
router.use('/product', productRouter);
router.use('/type', typeRouter);

module.exports = router;