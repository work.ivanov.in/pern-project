const Router = require('express');
const router = new Router();
const expirationController = require('../controllers/expirationController');
const checkRole = require('../middleware/checkRoleMiddleware');

router.post('/', checkRole('ADMIN'), expirationController.create);
router.get('/', expirationController.getAll);
router.delete('/', expirationController.delete);

module.exports = router;