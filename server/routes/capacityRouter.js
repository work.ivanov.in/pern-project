const Router = require('express');
const router = new Router();
const capacityController = require('../controllers/capacityController');
const checkRole = require('../middleware/checkRoleMiddleware');

router.post('/', checkRole('ADMIN'), capacityController.create);
router.get('/', capacityController.getAll);
router.delete('/', capacityController.delete);

module.exports = router;