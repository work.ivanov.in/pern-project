const {Aisle} = require('../models/models');
const ApiError = require('../error/ApiError');

class AisleController {
  async create(req, res) {
    const {aisleName} = req.body;
    const name = await Aisle.create({aisleName});
    res.json(name);
  }

  async getAll(req, res) {
    const aisles = await Aisle.findAll();
    return res.json(aisles);
  }

  async delete(req, res) {
    
  }
};

module.exports = new AisleController;