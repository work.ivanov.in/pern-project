const {CapacityTypes} = require('../models/models');
const ApiError = require('../error/ApiError');

class CapacityController {
  async create(req, res) {
    const {capacityType} = req.body;
    const type = await CapacityTypes.create({capacityType});
    res.json(type);
  }

  async getAll(req, res) {
    const capacity = await CapacityTypes.findAll();
    return res.json(capacity);
  }

  async delete(req, res) {
    
  }
};

module.exports = new CapacityController;