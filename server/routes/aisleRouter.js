const Router = require('express');
const router = new Router();
const aisleController = require('../controllers/aisleController');
const checkRole = require('../middleware/checkRoleMiddleware');


router.post('/', checkRole('ADMIN'), aisleController.create);
router.get('/', aisleController.getAll);
router.delete('/', aisleController.delete);

module.exports = router;