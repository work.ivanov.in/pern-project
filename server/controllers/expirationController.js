const {Expiration} = require('../models/models');
const ApiError = require('../error/ApiError');

class ExpirationController {
  async create(req, res) {
    const {expirationType} = req.body;
    const type = await Expiration.create({expirationType});
    res.json(type);
  }

  async getAll(req, res) {
    const expirations = await Expiration.findAll();
    return res.json(expirations);
  }

  async delete(req, res) {
    
  }
};

module.exports = new ExpirationController;